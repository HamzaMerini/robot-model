#include "umrob/robot_model.h"

#include <fmt/format.h>
#include <fmt/ostream.h>

#include <fstream>
#include <sstream>
#include <filesystem>

int main(int argc, char* argv[]) {
    auto binary_path = std::filesystem::path(argv[0]).parent_path();
    auto model_path =
        binary_path /
        std::filesystem::path("share/robot-model/models/nao_arms.urdf");
    std::ifstream model_file(model_path.generic_string());
    if (not model_file.is_open()) {
        fmt::print(stderr, "Failed to open the URDF file\n");
        std::exit(-1);
    }

    std::string urdf((std::istreambuf_iterator<char>(model_file)),
                     std::istreambuf_iterator<char>());

    auto model = umrob::RobotModel(urdf);

    model.jointPosition("LShoulderPitch") = 0;
    model.jointPosition("LShoulderRoll") = 0;
    model.jointPosition("LElbowYaw") = 0;
    model.jointPosition("LElbowRoll") = 0;
    model.jointPosition("LWristYaw") = 0;
    model.jointPosition("LHand") = 0;
    model.jointPosition("RShoulderPitch") = 0;
    model.jointPosition("RShoulderRoll") = 0;
    model.jointPosition("RElbowYaw") = 0;
    model.jointPosition("RElbowRoll") = 0;
    model.jointPosition("RWristYaw") = 0;
    model.jointPosition("RHand") = 0;

    model.update();

    auto r_gripper_pose = model.linkPose("r_gripper");
    fmt::print("r_gripper pose:\n{}\n", r_gripper_pose.matrix());

    auto r_gripper_jacobian = model.linkJacobian("r_gripper");
    fmt::print("r_gripper jacobian:\n{}\n", r_gripper_jacobian.matrix);

    auto l_gripper_pose = model.linkPose("l_gripper");
    fmt::print("l_gripper pose:\n{}\n", l_gripper_pose.matrix());

    auto l_gripper_jacobian = model.linkJacobian("l_gripper");
    fmt::print("l_gripper jacobian:\n{}\n", l_gripper_jacobian.matrix);
}