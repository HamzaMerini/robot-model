#include <fmt/core.h>
#include <umrob/robot_model.h>

#include <urdf_model/pose.h>
#include <urdf_parser/urdf_parser.h>
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <stdexcept>

namespace umrob {
RobotModel::RobotModel([[maybe_unused]] const std::string& urdf) {
    // TODO: implement
}

double& RobotModel::jointPosition(const std::string& name) {
    return joints_position_.at(name);
}

size_t RobotModel::jointIndex([[maybe_unused]] const std::string& name) const {
    // TODO: implement
    return 0;
}

const std::string& RobotModel::jointNameByIndex([
    [maybe_unused]] size_t index) const {
    return joints_position_.begin()->first; // TODO: implement
}

void RobotModel::update() {
    // TODO: implement
}

const Eigen::Affine3d&
RobotModel::linkPose(const std::string& link_name) const {
    return links_pose_.at(link_name);
}

const Jacobian& RobotModel::linkJacobian([
    [maybe_unused]] const std::string& link_name) {
    // TODO: implement

    return links_jacobian_.at(link_name);
}

const JointLimits&
RobotModel::jointLimits(const std::string& joint_name) const {
    return joints_limits_.at(joint_name);
}

size_t RobotModel::degreesOfFreedom() const {
    // TODO: implement
    return 0;
}

std::vector<urdf::JointConstSharedPtr> RobotModel::jacobianJointChain([
    [maybe_unused]] const std::string& link_name) const {
    std::vector<urdf::JointConstSharedPtr> joint_chain;

    // TODO: implement

    return joint_chain;
}

} // namespace umrob